# Finally, this is a script to build gcc.
# we build it from objdir/gcc, i.e cd .../objdir/gcc first.
#
# Note that:
# It has to use dependencies from locations defined by --prefix when we build them
# It has to refer to the same locations at runtime. We can optionally strip include and share from distribution tarball.
# Ideally, it has to pass 3-stage build and all tests. It takes long time; we can do it when building a final toolchain.
#
# temporary commented out:
#
# --with-local-prefix=/usr/_eca_tools/sysroot/2.2.5/i686_Linux/ \
#
export LD_LIBRARY_PATH=/usr/_eca_tools/mpfr/2.4.1/linux/lib:/usr/_eca_tools/gmp/4.3.1/linux/lib:/usr/_eca_tools/zlib/1.2.3/linux/lib:$LD_LIBRARY_PATH
CFLAGS=-m32 ../../gcc-4.3.4/configure \
--enable-multiarch \
--enable-multilib \
--enable-targets=all \
--build=i686-pc-linux-gnu \
--prefix=/usr/_eca_tools/gcc/4.3.4/linux \
--program-prefix=i686-pc-linux-gnu- \
--with-sysroot=/usr/_eca_tools/sysroot/2.2.5/i686_Linux/ \
--enable-languages=c,c++ \
--enable-threads=posix \
--enable-__cxa_atexit \
--enable-shared \
--disable-bootstrap \
--with-gnu-as \
--with-gnu-ld \
--with-as=/usr/_eca_tools/binutils/2.19.1/linux/bin/i686-pc-linux-gnu-as \
--with-ld=/usr/_eca_tools/binutils/2.19.1/linux/bin/i686-pc-linux-gnu-ld \
--with-mpfr=/usr/_eca_tools/mpfr/2.4.1/linux \
--with-gmp=/usr/_eca_tools/gmp/4.3.1/linux \
--with-zlib=/usr/_eca_tools/zlib/1.2.3/linux

make
sudo make install
