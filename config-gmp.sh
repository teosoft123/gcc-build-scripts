# GMP can be invoked from gcc's objdir like this:
# note that is you attempted a build for another ABI or acrhitecture, you need to
# remove everything in your objdir/gmp (rm -rf * works great)
(CFLAGS=-m32 ../../gmp-4.3.1/configure --prefix=/usr/_eca_tools/gmp/4.3.1/linux ABI=32; make; make check; sudo make install)
