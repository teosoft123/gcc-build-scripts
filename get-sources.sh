# This is a script to get sources for building gcc suite
# sources are coming from http://mirror.nexcess.net/gnu/

wget http://ftpmirror.gnu.org/binutils/binutils-2.19.1a.tar.bz2
wget http://ftpmirror.gnu.org/gcc/gcc-4.3.4/gcc-4.3.4.tar.gz
wget http://ftpmirror.gnu.org/mpfr/mpfr-2.4.1.tar.gz
wget http://ftpmirror.gnu.org/gmp/gmp-4.3.1.tar.gz
wget http://zlib.net/fossils/zlib-1.2.3.9.tar.gz
