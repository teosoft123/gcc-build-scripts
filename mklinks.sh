pushd gcc-4.3.4
ln -s ../mpfr-2.4.1 mpfr
ln -s ../gmp-4.3.1 gmp
ln -s ../binutils-2.19.1 binutils
# ln -s ../mpc-1.0.2 mpc
# ln -s ../isl-0.12.2 isl
# ln -s ../cloog-0.18.1 cloog
popd
