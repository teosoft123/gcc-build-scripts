# Bunutils build script
# Do I need the following?
# --with-sysroot=/usr/_eca_tools/sysroot
# --with-lib-path=//usr/_eca_tools/sysroot

(CFLAGS=-m32 ../../binutils-2.19.1/configure \
--build=i686-pc-linux-gnu \
--enable-multilib \
--enable-multiarch \
--enable-targets=all \
--prefix=/usr/_eca_tools/binutils/2.19.1/linux \
--with-sysroot=/usr/_eca_tools/sysroot/2.2.5/i686_Linux/ \
--program-prefix=i686-pc-linux-gnu- \
--enable-multilib \
--disable-werror; make; make check; sudo make install)
